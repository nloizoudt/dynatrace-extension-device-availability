# Dynatrace Extension - Device Availability 

# Pre-Requisites
- Dynatrace ActiveGate installed on a Windows Host

# Installation
- Download ZIP file and extract the contents
- Deploy Zip file as an Extension in Dynatrace
    - [Guide Here](https://www.dynatrace.com/support/help/shortlink/deploy_custom_extension#deploy-via-dynatrace-web-ui)
- Copy the extracted Folder to the ActiveGate plugin directory
    - Usually: C:\Program Files\dynatrace\remotepluginmodule\plugin_deployment
- Access the custom.remote.python.device_availibility folder and edit "Custom_Devices.csv"
    - The information is used as follows: 
        - Device Name
            - Name of the Device in Dynatrace (whitespace will be removed and replaced with an "_")
        - IP Address/DNS Name
            - The address of the device you would like to ping
        - Device Type
            - Allows us to filter by Type in Custom Charting
- Set the Owner of the filer to "LOCAL SERVICE"
    - Right Click the file and click properties
    - Click Security and then Advanced
    - Select "Change" next to the current "Owner" towards the top of the pop up
    - Enter the name "LOCAL SERVICE" and select check name to validate
    - Click ok
- Provide the file path to "Custom_Devices.csv" to the Extension in the Dynatrace ui
- Provide a descriptive Endpoint Name
- Select the ActiveGate you added the folder to

#Usage
- All devices created will appear in the Technologies screen under "Custom Technology"
- The metric created can be found in the custom Charting section
 - Category: Technologies
 - Metric: avg.ping.time.milliseconds



        

